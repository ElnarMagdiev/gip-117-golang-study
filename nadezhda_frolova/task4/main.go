package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type EventDota struct {
	Teams []Team
}

type Team struct {
	TeamId        int     `json:"team_id"`
	Rating        float32 `json:"rating"`
	Wins          int     `json:"wins"`
	Losses        int     `json:"losses"`
	LastMatchTime int     `json:"last_match_time"`
	Name          string  `json:"name"`
	Tag           string  `json:"tag"`
	LogoUrl       string  `json:"logo_url"`
}

func (eventDota *EventDota) Un(unByte []byte) {
	err := json.Unmarshal(unByte, &eventDota.Teams)
	CheckError(err)
}

func RootRout(w http.ResponseWriter, r *http.Request) {
	_, err := fmt.Fprintf(w, "My first API v1")
	CheckError(err)
}

func (eventDota *EventDota) ListRout(w http.ResponseWriter, r *http.Request) {
	answer, err := json.Marshal(eventDota.Teams)
	CheckError(err)
	_, err = w.Write(answer)
	CheckError(err)
}

func (eventDota *EventDota) ListFirstRout(w http.ResponseWriter, r *http.Request) {
	answer, err := json.Marshal(eventDota.Teams[0])
	CheckError(err)
	_, err = w.Write(answer)
	CheckError(err)
}

func (eventDota *EventDota) ListLatestRout(w http.ResponseWriter, r *http.Request) {
	answer, err := json.Marshal(eventDota.Teams[len(eventDota.Teams)-1])
	CheckError(err)
	_, err = w.Write(answer)
	CheckError(err)
}

func main() {
	var eventDota = EventDota{}
	file, er := http.Get("https://api.opendota.com/api/teams")
	CheckError(er)
	bytes, err := ioutil.ReadAll(file.Body)
	CheckError(err)
	eventDota.Un(bytes)
	http.HandleFunc("/", RootRout)
	http.HandleFunc("/list", eventDota.ListRout)
	http.HandleFunc("/list/first", eventDota.ListFirstRout)
	http.HandleFunc("/list/latest", eventDota.ListLatestRout)
	err1 := http.ListenAndServe(":8080", nil)
	CheckError(err1)
}

func CheckError(er error) {
	if er != nil {
		panic(er)
	}
}
