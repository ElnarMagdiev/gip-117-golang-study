package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

type Users struct {
	Users []User `json:"users"`
}

type User struct {
	Name   string `json:"name"`
	Type   string `json:"type"`
	Age    int    `json:"Age"`
	Social Social `json:"social"`
}

type Social struct {
	Facebook string `json:"facebook"`
	Twitter  string `json:"twitter"`
}

func main() {

	byteValue, _ := ioutil.ReadFile("example.json")

	var users Users

	_ = json.Unmarshal(byteValue, &users)

	for _, user := range users.Users {
		fmt.Println("User Name: ", user.Name)
		fmt.Println("	|->	User Type: ", user.Type)
		fmt.Println("	|->	User Age: ", user.Age)
		fmt.Println("		|->	User Facebook: ", user.Social.Facebook)
		fmt.Println("		|->	User Twitter: ", user.Social.Twitter)
	}

}
