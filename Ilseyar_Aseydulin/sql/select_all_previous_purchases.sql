SELECT u.*,
       prod.*,
       purc.*
FROM user_account AS u
       JOIN purchase AS purc ON purc.user_id = u.id AND purc.purchase_date < '2019-04-24 15:21:00'
       JOIN product AS prod ON purc.product_id = prod.id;
