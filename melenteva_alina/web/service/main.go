package main

import (
	"../../db_provider"
	"../../utils"
	"database/sql"
	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
	"net/http"
)

func main() {
	dataSource := "user=postgres password=root dbname=mydbkdd sslmode=disable"
	db, err := sql.Open("postgres", dataSource)
	C := db_provider.Server{db}
	utils.CheckError(err)
	router := mux.NewRouter()
	router.HandleFunc("/products", C.ProductsHandler).Methods("GET")
	router.HandleFunc("/products/id/{id}", C.ProductByIdHandler).Methods("GET")
	router.HandleFunc("/products/id/{id}", C.ProductUpdateHandler).Methods("PUT")
	router.HandleFunc("/products/id/{id}", C.ProductDeleteHandler).Methods("DELETE")
	router.HandleFunc("/products/id/{id}", C.ProductAddHandler).Methods("POST")
	http.Handle("/", router)
	_ = http.ListenAndServe(":8080", nil)
}
