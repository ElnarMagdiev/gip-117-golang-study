CREATE TABLE purchase
(
  id            SERIAL primary key                         NOT NULL,
  user_id       INTEGER REFERENCES user_account (id)       NOT NULL,
  product_id    INTEGER REFERENCES product (id)            NOT NULL,
  purchase_date TIMESTAMP CHECK ( purchase_date <= NOW() ) NOT NULL DEFAULT now()
);
