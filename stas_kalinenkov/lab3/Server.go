package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)


type User struct {
	Name   string `json:"name"`
	Type   string `json:"type"`
	Age    int    `json:"Age"`
	Social Social `json:"social"`
}

type Social struct {
	Facebook string `json:"facebook"`
	Twitter  string `json:"twitter"`
}

type example struct {
	Users []User `json:"users"`
}

func handler(w http.ResponseWriter, r *http.Request) {
	var data []byte
	data, _ = ioutil.ReadFile("example.json")

	var str example
	_ = json.Unmarshal(data, &str)

	arr := example{}
	error := json.Unmarshal(data, &arr)

	if error != nil {
		log.Printf("Unmarshaled: %v", error)
	}

	a, err := json.Marshal(arr.Users[0])
	f, err := json.Marshal(arr.Users[1])

	if err != nil {
		panic(err)
	}

	if r.URL.Path[1:] == "User№1" {
		w.Write(a)
	} else if r.URL.Path[1:] == "User№2" {
		w.Write(f)
	} else {
		w.WriteHeader(404)
	}

}


func main() {
	http.HandleFunc("/", Server)
	log.Fatal(http.ListenAndServe(":9090", nil))
}
