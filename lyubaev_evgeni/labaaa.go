package main
import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

func main() {
	bytes, err := ioutil.ReadFile("example.json")
	if err != nil {
		fmt.Println(err)
	} else {
		var example example
		err = json.Unmarshal(bytes, &example)
		if err != nil {
			fmt.Println(err)
		} else {
			for _, user := range example.Users {
				fmt.Println(user.Name, user.Type, user.Age, user.Social.Facebook, user.Social.Twitter)
			}

		}
	}
}

type User struct {
	Name   string `json:"name"`
	Type   string `json:"type"`
	Age    int    `json:"age"`
	Social social `json:"social"`
}

type social struct {
	Facebook string `json:"facebook"`
	Twitter  string `json:"twitter"`
}

type example struct {
	Users []User `json:"users"`
}

