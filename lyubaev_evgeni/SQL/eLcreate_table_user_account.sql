CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE table user_account
(
  id          SERIAL primary key,
  firstname   varchar(30) check (firstname <> lastname) not null,
  lastname    varchar(60) check (lastname <> firstname) not null,
  age         int check ( age >= 0 and age <= 150)      not null,
  external_id uuid default uuid_generate_v4()
);
